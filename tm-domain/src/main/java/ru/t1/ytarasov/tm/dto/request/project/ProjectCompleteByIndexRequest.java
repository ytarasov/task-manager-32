package ru.t1.ytarasov.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    private int projectIndex;

}
