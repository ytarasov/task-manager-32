package ru.t1.ytarasov.tm.api.endpoint;

import lombok.SneakyThrows;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ytarasov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpoint {
    @SneakyThrows
    UserLoginResponse login(UserLoginRequest request);

    @SneakyThrows
    UserLogoutResponse logout(UserLogoutRequest request);

    @SneakyThrows
    UserProfileResponse profile(UserProfileRequest request);
}
