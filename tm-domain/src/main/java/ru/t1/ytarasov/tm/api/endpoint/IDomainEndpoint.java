package ru.t1.ytarasov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.request.data.*;
import ru.t1.ytarasov.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    @SneakyThrows
    DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request);

    @NotNull
    @SneakyThrows
    DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request);

    @NotNull
    @SneakyThrows
    DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request);

    @NotNull
    @SneakyThrows
    DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request);

    @NotNull
    @SneakyThrows
    DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request);

    @NotNull
    @SneakyThrows
    DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request);

    @NotNull
    @SneakyThrows
    DataJsonFasterXmlLoadResponse dataJsonFasterXmlLoad(@NotNull DataJsonFasterXmlLoadRequest request);

    @NotNull
    @SneakyThrows
    DataJsonFasterXmlSaveResponse dataJsonFasterXmlSave(@NotNull DataJsonFasterXmlSaveRequest request);

    @NotNull
    @SneakyThrows
    DataJsonJaxbLoadResponse dataJsonJaxbLoad(@NotNull DataJsonJaxbLoadRequest request);

    @NotNull
    @SneakyThrows
    DataJsonJaxbSaveResponse dataJsonJaxbSave(@NotNull DataJsonJaxbSaveRequest request);

    @NotNull
    @SneakyThrows
    DataXmlFasterXmlLoadResponse dataXmlFasterXmlLoad(@NotNull DataXmlFasterXmlLoadRequest request);

    @NotNull
    @SneakyThrows
    DataXmlFasterXmlSaveResponse dataXmlFasterXmlSave(@NotNull DataXmlFasterXmlSaveRequest request);

    @NotNull
    @SneakyThrows
    DataXmlJaxbLoadResponse dataXmlJaxbLoad(@NotNull DataXmlJaxbLoadRequest request);

    @NotNull
    @SneakyThrows
    DataXmlJaxbSaveResponse dataXmlJaxbSave(@NotNull DataXmlJaxbSaveRequest request);

    @NotNull
    @SneakyThrows
    DataYamlLoadResponse dataYamlLoad(@NotNull DataYamlLoadRequest request);

    @NotNull
    @SneakyThrows
    DataYamlSaveResponse dataYamlSave(@NotNull DataYamlSaveRequest request);

}
