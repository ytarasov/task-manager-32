package ru.t1.ytarasov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListResponse taskList(@NotNull TaskListRequest request);

    @NotNull
    TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull
    TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskClearResponse taskClear(@NotNull TaskClearRequest request);

}
