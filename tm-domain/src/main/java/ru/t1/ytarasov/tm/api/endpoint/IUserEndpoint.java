package ru.t1.ytarasov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.request.user.*;
import ru.t1.ytarasov.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    @SneakyThrows
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

    @NotNull
    @SneakyThrows
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    @SneakyThrows
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    @SneakyThrows
    UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    @SneakyThrows
    UserProfileResponse userProfile(@NotNull UserProfileRequest request);

    @NotNull
    @SneakyThrows
    UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    @SneakyThrows
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

}
