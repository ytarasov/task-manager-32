package ru.t1.ytarasov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    @SneakyThrows
    ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request);

    @NotNull
    @SneakyThrows
    ProjectListResponse projectList(@NotNull ProjectListRequest request);

    @NotNull
    @SneakyThrows
    ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectCompleteByIdResponse projectCompleteById(ProjectCompleteByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectCompleteByIndexResponse projectCompleteByIndex(ProjectCompleteByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    @SneakyThrows
    ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    @SneakyThrows
    ProjectClearResponse projectClear(@NotNull ProjectClearRequest request);

}
