package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.response.task.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse taskList(@NotNull TaskListRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> tasks = taskService.findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        checkUser(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        checkUser(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.findOneByIndex(userId, taskIndex);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = taskService.findAllTasksByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = taskService.changeTaskStatusByIndex(userId, taskIndex, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = taskService.updateByIndex(userId, taskIndex, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = taskService.removeById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request) {
        @Nullable final String userId = request.getUserId();
        final int taskIndex = request.getTaskIndex();
        @Nullable final Task task = taskService.removeByIndex(userId, taskIndex);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse taskClear(@NotNull TaskClearRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

}
