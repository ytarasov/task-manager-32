package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.dto.request.user.*;
import ru.t1.ytarasov.tm.dto.response.user.*;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final User user = userService.create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse userLock(@NotNull UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userService.lockUser(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userService.unlockUser(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = userService.setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse userProfile(@NotNull UserProfileRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = userService.findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final User user = userService.updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse userRemove(@NotNull UserRemoveRequest request) {
        checkUser(request);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = userService.removeByLogin(login);
        return new UserRemoveResponse(user);
    }

}
