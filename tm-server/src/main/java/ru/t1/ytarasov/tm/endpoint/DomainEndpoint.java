package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ytarasov.tm.api.service.IDomainService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.data.*;
import ru.t1.ytarasov.tm.dto.response.data.*;
import ru.t1.ytarasov.tm.enumerated.Role;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse dataJsonFasterXmlLoad(@NotNull DataJsonFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }


    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse dataJsonFasterXmlSave(@NotNull DataJsonFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse dataJsonJaxbLoad(@NotNull DataJsonJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse dataJsonJaxbSave(@NotNull DataJsonJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @Override
    @SneakyThrows
    public @NotNull DataXmlFasterXmlLoadResponse dataXmlFasterXmlLoad(@NotNull DataXmlFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse dataXmlFasterXmlSave(@NotNull DataXmlFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse dataXmlJaxbLoad(@NotNull DataXmlJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse dataXmlJaxbSave(@NotNull DataXmlJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadResponse dataYamlLoad(@NotNull DataYamlLoadRequest request) {
        check(request, Role.ADMIN);
        domainService.loadDataYaml();
        return new DataYamlLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveResponse dataYamlSave(@NotNull DataYamlSaveRequest request) {
        check(request, Role.ADMIN);
        domainService.saveDataYaml();
        return new DataYamlSaveResponse();
    }

}
