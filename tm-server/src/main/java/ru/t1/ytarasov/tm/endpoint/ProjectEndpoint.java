package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectService;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.response.project.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse projectList(@NotNull ProjectListRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Project> projects = projectService.findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project = projectService.findOneByIndex(userId, projectIndex);
        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project =
                projectService.changeProjectStatusByIndex(userId, projectIndex, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse projectCompleteById(ProjectCompleteByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse projectCompleteByIndex(ProjectCompleteByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project =
                projectService.changeProjectStatusByIndex(userId, projectIndex, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = projectService.changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = projectService.changeProjectStatusByIndex(userId, projectIndex, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = projectService.updateByIndex(userId, projectIndex, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request) {
        checkUser(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Project project = projectTaskService.removeProjectById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        checkUser(request);
        @Nullable final String userId = request.getUserId();
        final int projectIndex = request.getProjectIndex();
        @Nullable final Project project = projectService.removeByIndex(userId, projectIndex);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse projectClear(@NotNull ProjectClearRequest request) {
        checkUser(request);
        @NotNull IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String userId = request.getUserId();
        projectTaskService.clearAllProjects(userId);
        return new ProjectClearResponse();
    }

}
