package ru.t1.ytarasov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    @SneakyThrows
    Domain loadDataBackup();

    @NotNull
    @SneakyThrows
    Domain saveDataBackup();

    @NotNull
    @SneakyThrows
    Domain loadDataBase64();

    @NotNull
    @SneakyThrows
    Domain saveDataBase64();

    @NotNull
    @SneakyThrows
    Domain loadDataBinary();

    @NotNull
    @SneakyThrows
    Domain saveDataBinary();

    @NotNull
    @SneakyThrows
    Domain loadDataJsonFasterXml();

    @NotNull
    @SneakyThrows
    Domain saveDataJsonFasterXml();

    @NotNull
    @SneakyThrows
    Domain loadDataJsonJaxb();

    @NotNull
    @SneakyThrows
    Domain saveDataJsonJaxb();

    @NotNull
    @SneakyThrows
    Domain loadDataXmlFasterXml();

    @NotNull
    @SneakyThrows
    Domain saveDataXmlFasterXml();

    @NotNull
    @SneakyThrows
    Domain loadDataXmlJaxb();

    @NotNull
    @SneakyThrows
    Domain saveDataXmlJaxb();

    @NotNull
    @SneakyThrows
    Domain loadDataYaml();

    @NotNull
    @SneakyThrows
    Domain saveDataYaml();

}
