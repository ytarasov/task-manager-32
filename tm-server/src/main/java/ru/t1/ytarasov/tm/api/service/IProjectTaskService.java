package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

    @NotNull
    Project removeProjectById(@Nullable String userId, @Nullable String projectId) throws AbstractException;

    @NotNull
    Project removeProjectByIndex(@Nullable String userId, int projectIndex) throws AbstractException;

    void clearAllProjects(@Nullable String userId) throws AbstractException;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

}
