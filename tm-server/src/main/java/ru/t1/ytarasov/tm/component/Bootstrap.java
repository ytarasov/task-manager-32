package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.*;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.dto.request.data.*;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.ytarasov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.request.user.*;
import ru.t1.ytarasov.tm.endpoint.*;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.repository.UserRepository;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_NAME = "ru.t1.ytarasov.tm.command";

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(DataBackupLoadRequest.class, domainEndpoint::dataBackupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::dataBackupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::dataBinarySave);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::dataJsonFasterXmlLoad);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::dataJsonFasterXmlSave);
        server.registry(DataJsonJaxbLoadRequest.class, domainEndpoint::dataJsonJaxbLoad);
        server.registry(DataJsonJaxbSaveRequest.class, domainEndpoint::dataJsonJaxbSave);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::dataXmlFasterXmlLoad);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::dataXmlFasterXmlSave);
        server.registry(DataXmlJaxbLoadRequest.class, domainEndpoint::dataXmlJaxbLoad);
        server.registry(DataXmlJaxbSaveRequest.class, domainEndpoint::dataXmlJaxbSave);
        server.registry(DataYamlLoadRequest.class, domainEndpoint::dataYamlLoad);
        server.registry(DataYamlSaveRequest.class, domainEndpoint::dataYamlSave);

        server.registry(UserRegistryRequest.class, userEndpoint::userRegistry);
        server.registry(UserChangePasswordRequest.class, userEndpoint::userChangePassword);
        server.registry(UserLockRequest.class, userEndpoint::userLock);
        server.registry(UserUnlockRequest.class, userEndpoint::userUnlock);
        server.registry(UserProfileRequest.class, userEndpoint::userProfile);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::userUpdateProfile);
        server.registry(UserRemoveRequest.class, userEndpoint::userRemove);

        server.registry(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.registry(ProjectListRequest.class, projectEndpoint::projectList);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::projectShowById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::projectShowByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::projectStartById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::projectStartByIndex);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::projectCompleteById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::projectCompleteByIndex);
        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);

        server.registry(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.registry(TaskListRequest.class, taskEndpoint::taskList);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::taskBindToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::taskUnbindFromProject);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::taskShowById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::taskShowByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::taskShowByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::taskStartById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::taskStartByIndex);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::taskCompleteById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::taskCompleteByIndex);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::taskClear);

        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);
    }

    private void initBackup() {
        backup.start();
    }

    private void prepareStart() {
        initPid();
        initUsers();
        initLogger();
        initDemoData();
        initBackup();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        server.stop();
    }

    public void run() {
        prepareStart();
    }


    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        try {
            userService.create("TEST", "TEST", "test@mail.ru");
            userService.create("ADMIN", "ADMIN", Role.ADMIN);
        } catch (@NotNull final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void initDemoData() {
        try {
            @NotNull Project project = new Project("p1", "pp1");
            project.setStatus(Status.IN_PROGRESS);
            project.setUserId(userService.findByLogin("TEST").getId());
            projectRepository.add(project);
            @NotNull Project project1 = new Project("p2", "pp2");
            project1.setStatus(Status.IN_PROGRESS);
            project1.setUserId(userService.findByLogin("ADMIN").getId());
            projectRepository.add(project1);

            @NotNull Task task = new Task("t1", "tt1");
            task.setStatus(Status.IN_PROGRESS);
            task.setProjectId(project.getId());
            task.setUserId(userService.findByLogin("TEST").getId());
            taskRepository.add(task);
            @NotNull Task task1 = new Task("t2", "tt2");
            task1.setStatus(Status.IN_PROGRESS);
            task1.setProjectId(project.getId());
            task1.setUserId(userService.findByLogin("ADMIN").getId());
            taskRepository.add(task1);
        } catch (final @Nullable Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
