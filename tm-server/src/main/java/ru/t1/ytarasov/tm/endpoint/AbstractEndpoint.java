package ru.t1.ytarasov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public User checkUser(AbstractUserRequest request) throws AbstractException {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    protected void check(AbstractUserRequest request, Role role) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        @NotNull final User user = checkUser(request);
        if (user.getRole() != role) throw new AccessDeniedException();
    }
}
