package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.IEndpointClient;
import ru.t1.ytarasov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}
