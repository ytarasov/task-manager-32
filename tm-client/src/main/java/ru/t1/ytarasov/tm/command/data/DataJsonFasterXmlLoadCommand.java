package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataJsonFasterXmlLoadRequest;

public final class DataJsonFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-fasterxml-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from json by FasterXml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON FASTERXML LOAD]");
        getDomainEndpoint().dataJsonFasterXmlLoad(new DataJsonFasterXmlLoadRequest());
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
