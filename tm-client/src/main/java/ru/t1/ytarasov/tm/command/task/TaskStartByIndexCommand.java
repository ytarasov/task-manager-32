package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setTaskIndex(index);
        getTaskEndpoint().taskStartByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
