package ru.t1.ytarasov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;

public final class DisconnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        try {
            serviceLocator.getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            serviceLocator.getLoggerService().error(e);
        }
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }
}
