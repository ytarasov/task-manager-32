package ru.t1.ytarasov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IEndpointClient;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;
import java.net.Socket;

public final class ConnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server";

    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        try {
            @NotNull final IEndpointClient endpointClient = getServiceLocator().getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();

            serviceLocator.getAuthEndpoint().setSocket(socket);
            serviceLocator.getSystemEndpoint().setSocket(socket);
            serviceLocator.getDomainEndpoint().setSocket(socket);
            serviceLocator.getProjectEndpoint().setSocket(socket);
            serviceLocator.getTaskEndpoint().setSocket(socket);
            serviceLocator.getUserEndpoint().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }
}
