package ru.t1.ytarasov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.client.AuthEndpointClient;
import ru.t1.ytarasov.tm.client.UserEndpointClient;
import ru.t1.ytarasov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public UserEndpointClient getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    public AuthEndpointClient getAuthEndpoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
