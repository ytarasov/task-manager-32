package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataJsonJaxbSaveRequest;

public final class DataJsonJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-jaxb-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to json by JAXB";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON JAXB SAVE]");
        getDomainEndpoint().dataJsonJaxbSave(new DataJsonJaxbSaveRequest());
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
