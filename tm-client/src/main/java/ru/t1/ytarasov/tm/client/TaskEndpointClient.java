package ru.t1.ytarasov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.response.task.*;

public class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse taskList(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse taskClear(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

}
