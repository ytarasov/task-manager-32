package ru.t1.ytarasov.tm.client;

import lombok.SneakyThrows;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ytarasov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserProfileResponse;

public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    @Override
    @SneakyThrows
    public UserLoginResponse login(UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @SneakyThrows
    public UserLogoutResponse logout(UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @SneakyThrows
    public UserProfileResponse profile(UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
