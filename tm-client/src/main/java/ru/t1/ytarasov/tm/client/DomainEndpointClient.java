package ru.t1.ytarasov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ytarasov.tm.dto.request.data.*;
import ru.t1.ytarasov.tm.dto.response.data.*;

public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse dataJsonFasterXmlLoad(@NotNull DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse dataJsonFasterXmlSave(@NotNull DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse dataJsonJaxbLoad(@NotNull DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse dataJsonJaxbSave(@NotNull DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull DataXmlFasterXmlLoadResponse dataXmlFasterXmlLoad(@NotNull DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse dataXmlFasterXmlSave(@NotNull DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse dataXmlJaxbLoad(@NotNull DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse dataXmlJaxbSave(@NotNull DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadResponse dataYamlLoad(@NotNull DataYamlLoadRequest request) {
        return call(request, DataYamlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveResponse dataYamlSave(@NotNull DataYamlSaveRequest request) {
        return call(request, DataYamlSaveResponse.class);
    }

}
